package com.yeele.babynamer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 *
 */
public class BabyNamer 
{
	public Document getDocument(String url) throws IOException{
		
		Document doc = null;
		int trial = 3;
		
		try{
			for(int i=0; i < trial; i++){
				Connection conn = Jsoup.connect(url);
				doc = conn.get();
				return doc;
			}
			
		} catch (SocketTimeoutException e) {
			try {
				Thread.sleep(10000); // wait for 10 secs
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		return doc;
		
	}
	
	
}
