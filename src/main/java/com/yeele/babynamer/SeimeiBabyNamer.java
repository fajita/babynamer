package com.yeele.babynamer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TreeMap;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SeimeiBabyNamer extends BabyNamer 
{
	
	private String requestUrl = "http://seimei.linkstudio.biz/?sex=m&surname=%s&firstname=%s";
	private Hashtable<Integer, ArrayList<String>> map = new Hashtable<Integer, ArrayList<String>>();
	
	public SeimeiBabyNamer(){
    	super();
    }
    
    public String getURL(String lastname, String firstname){
    	String url = String.format(this.requestUrl, lastname, firstname);
    	return url;
	}
    
    public int lookupFortune(String lastname, String firstname) throws IOException{
    	
    	StringBuffer sb = new StringBuffer();
    	int score = 0;
    	String url = this.getURL(lastname, firstname);
    	Document doc = this.getDocument(url);
    	if(doc != null){
    		
        	Elements results = doc.select(".kekka h3");
    		for(Element result : results){
    			Elements entry = result.select("strong");
    			for(Element fortune : entry){
    				String text = fortune.text();
    				sb.append(text);
    				score += this.getScore(fortune.text());
    			}
    			sb.append(", ");
    		}
    		url = "<a href=" + url + ">link</a>"; 
    		sb.insert(0, lastname + " " + firstname + " " + score + " " + url + " ");
    		
    		String overview = sb.toString();
    		if(!map.containsKey(score)){
    			map.put(score, new ArrayList<String>());
    		}
    		ArrayList<String> overviews = map.get(score);
    		overviews.add(overview);
    		map.put(score, overviews);
    		
    		System.out.println(sb.toString());
    	}
    	
		return score;
	}
    
    private int getScore(String fortune){
    	if(fortune.equals("大吉")) return 100;
    	else if(fortune.equals("吉")) return 80;
    	else if(fortune.equals("中吉")) return 75;
    	else if(fortune.equals("小吉")) return 65;
    	else if(fortune.equals("半吉")) return 60;
    	else if(fortune.equals("末吉")) return 55;
    	else if(fortune.equals("平")) return 50;
    	else if(fortune.equals("区")) return 40;
    	else if(fortune.equals("小区")) return 30;
    	else if(fortune.equals("半区")) return 20;
    	else if(fortune.equals("大区")) return 10;
    	else return 0;
    }
    
    public void write(FileWriter fw) throws IOException{
    	Enumeration keys = this.map.keys();
    	while(keys.hasMoreElements()){
    		Object key = keys.nextElement();
    		for(String result : this.map.get(key)){
    			fw.write(result);
    			//fw.write("\r\n");
    			fw.write("<br>");
    		}
    	}
    	fw.close();
    }
    
}
